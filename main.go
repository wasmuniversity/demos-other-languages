package main

import (
	"fmt"
	//"log"
	"os"

	"github.com/suborbital/reactr/rt"
	"github.com/suborbital/reactr/rwasm"
)

func main() {
	calledFunction := os.Args[1]
	wasmModule := os.Args[1] + "/" + os.Args[1] + ".wasm"
	
	parameters := os.Args[2:][0]
	//log.Println(os.Args)
	
	r := rt.New()
	
	wasmFunction := r.Register(calledFunction, rwasm.NewRunner(wasmModule))
	
	result, err := wasmFunction(parameters).Then()
	if err != nil {
		fmt.Println(err)
		return
	}
	
	fmt.Println(string(result.([]byte)))

}
